// Write TypeScript code!
const appDiv: HTMLElement = document.getElementById('app');
appDiv.innerHTML = `<h1>TypeScript Starter TEST</h1>`;
const lista: HTMLElement = document.getElementById('lista');
const btn: HTMLElement = document.getElementById('btn');
btn.onclick = function submit() {
  const nume: HTMLElement = document.getElementById('nume');
  const prenume: HTMLElement = document.getElementById('prenume');
  const nota: HTMLElement = document.getElementById('nota');
  lista.innerHTML += (`<li>` + (nume as HTMLInputElement).value + `</li>` + `<li>` + (prenume as HTMLInputElement).value + `</li>` + `<li>` + (nota as HTMLInputElement).value + `</li>`);
};
